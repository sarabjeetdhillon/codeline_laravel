<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = \App\Film::orderBy('id','desc')->paginate('1');
        // echo $app_path = storage_path('app');
        // die;

        $view_elements = [];
        $view_elements['films'] = $films;
        $view_elements['page_title'] = "All Films";

        return view('films.all', $view_elements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view_elements = [];
        $view_elements['page_title'] = "Add New Film";

        return view('films.create', $view_elements);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\FilmsRequest $request)
    {
        $return_path = $request->file('pic')->store('public/films');
        $path = str_replace("public/","",$return_path);

        \App\Film::create([
            'name'=>$request->name,
            'description'=>$request->description,
            'slug'=>str_slug($request->name),
            'release_date'=>$request->release_date,
            'rating'=>$request->rating,
            'ticket_price'=>$request->ticket_price,
            'country'=>$request->country,
            'genre'=>$request->genre,
            'pic'=>$path,
         ]);

         return redirect('/films')->with("success","Film added successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $film = \App\Film::with('comments')->where('slug',$slug)->first();
        $view_elements = [];
        $view_elements['film'] = $film ;
        $view_elements['page_title'] = $film->name;

        return view('films.single', $view_elements);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $view_elements = [];
        $view_elements['page_title'] = "All Films";

        return view('films.all', $view_elements);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    

}
