<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'description'=>'required',
            'release_date'=>'required',
            'country'=>'required',
            'rating'=>'required|numeric|between:1,5',
            'ticket_price'=>'required',
            'genre'=>'required',
            'pic'=>'required|file',
        ];
    }
}
