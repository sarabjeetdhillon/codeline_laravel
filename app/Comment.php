<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['posted_by','comment_text','film_id','comment_status'];
}
