@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                        {{$film->name}} ({{$film->rating}})
                    <span class="pull-right">
                        <a href="javascript:window.history.back(); ">Back</a>   
                    </span>
                </div>

                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                   
                   <div class="row">
                        <div class="col-md-6">
                            <img src="{{ asset('storage/'.$film->pic) }}" width="100%" />
                        </div>   
                        
                        <div class="col-md-6">
                                <h3>{{$film->name}} ({{$film->rating}})</h3>
                            <p>{{$film->description}}</p>    
                            
                            <div class="row">
                                <div class="col-md-6">
                                        <p>{{$film->country}}</p>
                                        <p>{{$film->release_date}}</p>     
                                    </div>
                                    <div class="col-md-6">
                                            <p>{{$film->genre}}</p>
                                            <p>${{$film->ticket_price}}</p>    
                                    </div>
                            </div>
                            
                            <div class="row">
                                
                                @auth
                                    <h4>Post Comments</h4>
                                    <form class="form-horizontal" action="/comments" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{$film->id}}" name="film_id"/>
                                        <div class="form-group{{ $errors->has('posted_by') ? ' has-error' : '' }}">
                                                
                                                <div class="col-md-11">
                                                    <input id="posted_by" type="text" class="form-control" name="posted_by" value="{{ old('posted_by') }}" required autofocus>
                    
                                                    @if ($errors->has('posted_by'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('posted_by') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                    
                                            <div class="form-group{{ $errors->has('comment_text') ? ' has-error' : '' }}">
                                            
                    
                                                <div class="col-md-11">
                                                    <textarea class="form-control" name="comment_text" required >{{ old('comment_text') }}</textarea>
                                                    
                                                    @if ($errors->has('comment_text'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('comment_text') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Post Comment
                                                    </button>
                                                </div>
                                            </div>
                    

                                    </form>
                                @endauth
                                
                                @guest
                                 <p><b><a href="/login">Login</a></b> or <b><a href="/register">Register</a></b> to leave a comment.</p>   
                                @endguest

                                <h4>{{count($film->comments)}} Comments</h4>
                                <div class="row">
                                    @foreach($film->comments as $comment)
                                        <div class="col-md-12" style="margin-bottom:5px">
                                                <p><b>{{$comment->posted_by}} - {{format_date($comment->created_at,'3')}}</b></p>
                                                <p>{{$comment->comment_text}}</p>
                                        </div>
                                        
                                    @endforeach
                                </div>

                            </div>

                            

                            
                        </div>   

                    </div> 
                    

                    
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
