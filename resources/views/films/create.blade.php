@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Film

                    <span class="pull-right">
                        <a href="/films">Cancel</a>   
                    </span>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('/films') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"  autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <textarea class="form-control" name="description" >{{ old('description') }}</textarea>
                                
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('release_date') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Release Date</label>
    
                                <div class="col-md-6">
                                    <input id="release_date" type="date" class="form-control" name="release_date" value="{{ old('release_date') }}" >
    
                                    @if ($errors->has('release_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('release_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>

                        <div class="form-group{{ $errors->has('rating') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Rating</label>
    
                                <div class="col-md-6">
                                    <input id="rating" type="text" class="form-control" name="rating" value="{{ old('rating') }}" >
    
                                    @if ($errors->has('rating'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('rating') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>

                        <div class="form-group{{ $errors->has('ticket_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Ticket Price</label>
    
                                <div class="col-md-6">
                                    <input id="ticket_price" type="text" class="form-control" name="ticket_price" value="{{ old('ticket_price') }}" >
    
                                    @if ($errors->has('ticket_price'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ticket_price') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>

                        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                <label for="country" class="col-md-4 control-label">Country</label>
    
                                <div class="col-md-6">
                                    <input id="country" type="text" class="form-control" name="country" value="{{ old('country') }}" >
    
                                    @if ($errors->has('country'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('country') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>

                        <div class="form-group{{ $errors->has('genre') ? ' has-error' : '' }}">
                                <label for="genre" class="col-md-4 control-label">Genre</label>
    
                                <div class="col-md-6">
                                    <input id="genre" type="text" class="form-control" name="genre" value="{{ old('genre') }}" >
    
                                    @if ($errors->has('genre'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('genre') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>

                        <div class="form-group{{ $errors->has('pic') ? ' has-error' : '' }}">
                                <label for="pic" class="col-md-4 control-label">Pic</label>
    
                                <div class="col-md-6">
                                    <input id="pic" type="file" class="form-control" name="pic" value="{{ old('pic') }}" >
    
                                    @if ($errors->has('pic'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pic') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>



                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
