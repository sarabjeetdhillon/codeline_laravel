@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">All Films

                    <span class="pull-right">
                        <a href="/films/create">+ Add</a>   
                    </span>
                </div>

                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                   @foreach($films as $film) 
                   <div class="row">
                        <div class="col-md-6">
                            <img src="{{ asset('storage/'.$film->pic) }}" width="100%" />
                        </div>   
                        
                        <div class="col-md-6">
                            <h3>{{$film->name}} ({{$film->rating}})</h3>
                            <p>{{$film->description}}</p>    
                            <a href="/films/{{$film->slug}}">More Info</a>
                               
                                
                                
                            
                        </div>   

                    </div> 
                    @endforeach

                    
                </div>
                <div class="panel-heading">
                        <center><span>
                                {{ $films->links() }} 
                        </span>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
