<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Comment::truncate();
        $faker = Faker\Factory::create();
        
        for($i = 0; $i < 12; $i++) {
            \App\Comment::create([
                'posted_by' => $faker->name,
                'film_id' => $faker->randomElement(['1','2','3','4','5','6']) ,
                'comment_text' => $faker->text,                             
                'comment_status' => "published",
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
    }
}
